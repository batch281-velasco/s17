/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function basicInfo(){
		let fullName = prompt("Enter your full name:");
		let age = prompt("How old are you?");
		let location = prompt("Where do you live");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
	}

	basicInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favMusicArtists(){
		console.log("1. Hoyo Mix");
		console.log("2. Ben & Ben");
		console.log("3. Zack Tabudlo");
		console.log("4. The Carpenters");
		console.log("5. Up Dharma Down");
	}

	favMusicArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favMovies(){
		console.log("1. Avengers: Endgame");
		console.log("Rotten Tomatoes Rating: 94%");
		console.log("2. Avengers: Infinity War");
		console.log("Rotten Tomatoes Rating: 85%");
		console.log("3. The Hobbit: Desolation of Smaug");
		console.log("Rotten Tomatoes Rating: 74%");
		console.log("4. The Hunger Games: Catching Fire" );
		console.log("Rotten Tomatoes Rating: 90%");
		console.log("5. Godzilla vs. Kong");
		console.log("Rotten Tomatoes Rating: 75%");
	}

	favMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);
